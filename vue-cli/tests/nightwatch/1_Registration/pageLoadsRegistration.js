// As a user
// I would like to create an account
// So that I can use WT.Social

// Nightwatch DOES NOT LIKE LOCALHOST
// I never figured out why. Please let me know if you do!

// var cp = require('child_process');
// cp.exec('./delete.sh'); //works

const deleteTestUser = require("./deleteTestUser.js");

describe('Testing Registration', function() {

    beforeEach(() => {
        deleteTestUser.yourScript;
    }, )

    afterEach(browser => browser.end());

    test('Do all of the elements required to access the register function load?', function(browser) {
        browser
            .url('https://d2cgkfgsfyw731.cloudfront.net/')
            .verify.elementPresent(".AuthWidget")
            .verify.elementPresent(".V-Tab-Register")
            .click(".V-Tab-Register")
            .verify.elementPresent(".Register")
            // This input field is fname
            .assert.visible("input[id='input-48'")
            // This input field is lname
            .assert.visible("input[id='input-51'")
            // This input field is email
            .assert.visible("input[id='input-54'")
            // This input field is password1
            .assert.visible("input[id='input-57'")
            // This input field is password2
            .assert.visible("input[id='input-61'")
            // It doesn't appear to know that the registration button 
            // is class="regsiter" but it knows there is _a_ button
            .assert.visible('button[class="register v-btn v-btn--is-elevated v-btn--has-bg theme--light v-size--default teal lighten-2"]')
    });


});

//.waitForElementVisible('app-content', 10000000)
// .assert.cssClassPresent("#about")
//.expect.element('#about').to.be.present;
// browser.assert.cssClassPresent("#main", "container");
// .assert.titleContains('Ecosia')
// .assert.visible('input[type=search]')
// .setValue('input[type=search]', 'nightwatch')
// .assert.visible('button[type=registerBtn]')
// .click('button[type=submit]')