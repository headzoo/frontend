#!/bin/env python3
import json
import yaml
#import re
from pprint import pprint

# === DEFINE FILENAMES AND LANGUAGE CODES HERE ===
origfn = "enWebsiteCopy.json"
codes = ["en", "de", "es", "ptbr", "ru"]
# ================================================

# utility class for outputting multiline strings for volunteer yaml files
class MultiLineString(str):
    pass

# for different types of multiline string formats, change the style parameter
def represent_multiline(dumper, data):
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')

# add the custom class to PyYAML so it dumps accordingly
yaml.add_representer(MultiLineString, represent_multiline)

# first make sure the files are valid json syntax
for code in codes:
    if code == "ru":
        enc = "cp866"
    else:
        enc = "utf-8"
    fn = code+"WebsiteCopy.json"
    with open(fn, encoding=enc) as file:
        try:
            json.load(file)
        except ValueError as err:
            print("===================================")
            print(f"\nThere is a JSON error in {fn}...\nCheck the commas and quotation marks\n")
            print("===================================\n")
            raise(err)

# now let's create the dictionary of exemptions that don't need to be translated
exemptions = {code:[] for code in codes}
exemptions['en'].extend([
    'FAQS',
    'We provide a link on sign-up allowing you to invite people to follow you on WT.Social. The url is <code>https://wt.social/YOUR-USERNAME-HERE/followme</code>',
    'Jimmy Wales',
    'THESE ARE ALL HERE INSTEAD OF IN A SECTION BECAUSE WE NEED TO INTERPRET THE STRINGS IN RECENT CHANGES - RECENT CHANGES DESPERATELY NEEDS REFACTORING BUT UNTIL SUCH A POINT AS WE\'VE DONE THAT THESE HAVE TO LIVE OUTSIDE OF A SECTION',
    'SAVING UNTIL SUBWIKIS ARE IMPORTED - THEN WE SHOULD COLLABORATE ON THIS',
    'Six Steps to Creating a New Fact Check on WT.Social',
    'SHOULD DISCUSS'
])

# initialize empty dict to hold language data from json import
data = {}
# fetch the data from json and yaml, and put it all in the dict
for code in codes:
    if code == "ru":
        enc = "cp866"
    else:
        enc = "utf-8"
    data[code] = json.load(open(code+"WebsiteCopy.json", encoding=enc))
    if code != "en":
        data[code]["_PLEASE_TRANSLATE"] = {}
        try:
            data[code]["_TRANSLATED"] = yaml.safe_load(open(code+"Translated.yaml", encoding=enc))
        except:
            data[code]["_TRANSLATED"] = {}

# iterate over each item in the English copytext data
for k,v in data["en"].items():
    if v in exemptions["en"]:
        continue
    if isinstance(v, str):
        vtype = "String"
    if isinstance(v, dict):
        vtype = "Section"
    # iterate again to check each other language
    for code in codes[1:]:
        if k in data[code]:
            if data[code][k] == v:
                if k in data[code]["_TRANSLATED"]:
                    data[code][k] = data[code]["_TRANSLATED"][k]
                elif '\n' in v:
                    data[code]["_PLEASE_TRANSLATE"][k] = MultiLineString(v)
                else:
                    data[code]["_PLEASE_TRANSLATE"][k] = v
                print(f"Added {vtype} {k} to {code} translation request list")
            elif vtype == "Section":
                # if there is a section where v is different between en and other lang,
                # check each item within to make sure they are translated.
                for kk,vv in v.items():
                    if vv in exemptions["en"]:
                        continue
                    if kk in data[code][k]: # if this EN key is already in the other lang
                        if data[code][k][kk] == vv: # if it's untranslated
                            if k in data[code]["_TRANSLATED"] and kk in data[code]["_TRANSLATED"][k]:
                                data[code][k][kk] = data[code]["_TRANSLATED"][k][kk]
                            else:
                                if k not in data[code]["_PLEASE_TRANSLATE"].keys():
                                    data[code]["_PLEASE_TRANSLATE"][k] = {}
                                if '\n' in vv:
                                    data[code]["_PLEASE_TRANSLATE"][k][kk] = MultiLineString(vv)
                                else:
                                    data[code]["_PLEASE_TRANSLATE"][k][kk] = vv
                            print(f"Added {kk} to {code} translation request list")
                    else:
                        data[code][k][kk] = vv # if the EN key is not in the other lang yet
                        if k in data[code]["_TRANSLATED"] and kk in data[code]["_TRANSLATED"][k]:
                            data[code][k][kk] = data[code]["_TRANSLATED"][k][kk]
                        else:
                            if k not in data[code]["_PLEASE_TRANSLATE"].keys():
                                data[code]["_PLEASE_TRANSLATE"][k] = {}
                            if '\n' in vv:
                                data[code]["_PLEASE_TRANSLATE"][k][kk] = MultiLineString(vv)
                            else:
                                data[code]["_PLEASE_TRANSLATE"][k][kk] = vv
                            print(f"Added {kk} to {code} data and translation request list")
        else:
            if k in data[code]["_TRANSLATED"]:
                data[code][k] = data[code]["_TRANSLATED"][k]
            else:
                data[code][k] = v
                if '\n' in v:
                    data[code]["_PLEASE_TRANSLATE"][k] = MultiLineString(v)
                else:
                    data[code]["_PLEASE_TRANSLATE"][k] = v
                print(f"Added {vtype} {k} to {code} data and translation request list")

# write the compiled translations to file
for code in codes:
    if code == "ru":
        enc = "cp866"
    else:
        enc = "utf-8"
    if code is not "en":
        with open(code+"PleaseTranslate.yaml", "w", encoding=enc) as f:
            yaml.dump(data[code]["_PLEASE_TRANSLATE"], f, sort_keys=True, allow_unicode=True)
        data[code].pop("_PLEASE_TRANSLATE")
        data[code].pop("_TRANSLATED")
    with open(code+"WebsiteCopyNew.json", "w", encoding=enc) as f:
        json.dump(data[code], f, indent=2, sort_keys=True, ensure_ascii=False)
