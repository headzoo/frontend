import boto3
# import simplejson as json
import json
import sys

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

table_to = dynamodb.Table('LanguageTest')
limitby = 100

languages = [
    {'language': 'English', 'code': 'en'}, 
    {'language': 'Español', 'code': 'es'}, 
    {'language': 'Deutsch', 'code': 'de'}, 
    {'language': 'Português', 'code': 'ptbr'},
    {'language': 'Русский', 'code': 'ru'}
]

for i in languages:
    filename = './vue-cli/src/copy/' + i['code'] + 'WebsiteCopy.json'
    print(filename)
    languagefile = open(filename, "r", encoding="utf-8")
    load_language = json.load(languagefile)
    print(load_language)
    response = table_to.put_item(Item = load_language)