import axios from "axios"

export default {
  namespaced: true,
  state: { 
    archivingposts: false,
    postsArchived: false,
    debug: []
  },
  actions: {
    // ALERT_SPAM(payload){
    //     console.log("WE ARE ALERTING SPAM")  
    // },
    ARCHIVE_USER({commit, state, rootGetters}, payload){
      commit("CLEAR_DEBUG")
      state.archivingposts = true
      var url = process.env.VUE_APP_MODERATIONBASEURL + "archiveuser"
      var data = {
        "userslug": payload
      };
      axios
      .put(url, data, rootGetters["UserStore/authHeader"])
      .then(function () {
        state.archivingposts = false
        commit("POSTS_ARCHIVED")
      })
      .catch(function (error) {
        state.archivingposts = false
        console.log(error)  
        commit('ADD_DEBUG', "Could not archive posts")
      })
    }
  },
  mutations: {
    CLEAR_DEBUG(state){
      state.debug = []
    },
    POSTS_ARCHIVED(state){
      state.postsArchived = true
    }
  },
  getters: {
    GET_POST_ARCHIVE_DEBUG(state){
      return state.usergroups
    },
    GET_POST_ARCHIVE_STATUS(state){
      return state.postsArchived
    },
    GET_POST_ARCHIVING_STATUS(state){
      return state.archivingposts
    }
  }
}