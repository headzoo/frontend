import defaultprofilepics from '../../assets/defaultprofilepics.json'

export default {
  namespaced: true,
  state: {
    profilePicURLs: [],
    randomPicURL: {},
    chosenPic: {},
    picChosen: false
  },
  actions: {
    GET_PROFILE_URLS({commit}){
        commit("GET_PROFILE_URLS")
    },
    SET_CHOSEN_PIC({commit}, payload){
      commit("SET_CHOSEN_PIC", payload)
    },
    PICTURE_IS_CHOSEN({commit}, payload){
      commit("PICTURE_IS_CHOSEN", payload)
    }
},
  mutations: {
    PICTURE_IS_CHOSEN(state, payload){
      state.picChosen = payload
    },
    GET_PROFILE_URLS(state){
        let i = ""
        state.profilePicURLs = []
        for (i in defaultprofilepics){
          console.log(defaultprofilepics[i])
          let imgurl = process.env.VUE_APP_S3URL + 'defaultpictures/' + defaultprofilepics[i]
          let srckey = 'defaultpictures/' + defaultprofilepics[i]
          state.profilePicURLs.push({"imgurl": imgurl, "srckey": srckey})
        }
    },
    SET_CHOSEN_PIC(state, payload){
      state.chosenPic = payload
    }
},
getters: {
      GET_LIST_OF_PROFILE_PICS (state){
        return state.profilePicURLs
      },
      GET_RANDOM_PROFILE_PIC (state){
        let numberOfOptions = state.profilePicURLs.length
        let randomOption = Math.floor(Math.random() * numberOfOptions)
        state.randomPicURL = state.profilePicURLs[randomOption]
        return state.randomPicURL
      },
      GET_CHOSEN_PIC(state){
        return state.chosenPic
      },
      PICTURE_IS_CHOSEN(state){
        return state.picChosen
      }
}}