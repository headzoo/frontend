import axios from "axios";
import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    DefaultPreferences:  {      
      notification: true,
      emailDigest: true,
      emailNew: false,
    },
    followMemory: {
      subwiki: {},
      userprofile: {},
    },
    followOverviewFetching: false,
    followOverview: {
      "userSlug": null,
      "following-users": [],
      "followed-by-users": [],
      "following-subwikis": [],
    },
    HasLoaded: [],
    Contexts: {
      follow: {
        button: "Follow",
        label: "Follow text",
      },
    },
  },
  getters: {

    GET_MEMORY: (state) => {
      return state.followMemory;
    },
    GET_FOLLOW_OVERVIEW: (state) => {
      return state.followOverview;
    },
    GET_FOLLOW_OVERVIEW_FETCHING: (state) => {
      return state.followOverviewFetching;
    },
    GET_HAS_FOLLOW_LOADED: (state) => {
      return state.HasLoaded;
    },
    GET_FOLLOW_STATUS: (state) => (payload) => {

      if (payload.type && payload.slug) {
        if (state.followMemory[payload.type]) {
          return state.followMemory[payload.type][payload.slug];
        } else {
          console.log("Unrecognised type: " + payload.type);
        }
      }
    },
    FOLLOW_SLUG: (state, getters, rootState) => (payload) => {
      return payload.type +
        "_" +
        payload?.slug?.trim() +
        "_" +
        rootState.UserStore.userprofile.slug;
    }
  },
  mutations: {

    CLEAR_FOLLOW_MEMORY(state) {
      Vue.set(state.followMemory, 'subwiki', {});
      Vue.set(state.followMemory, 'userprofile', {});
    },
    CHANGE_FOLLOW_MEMORY(state, payload) {
      
      state.HasLoaded.push(payload.type + "/" + payload.followSlug);

      if (state.followMemory[payload.type]) {
        Vue.set(state.followMemory[payload.type], payload.followSlug, {
          key: payload.key,          
          label: payload.followLabel,          
          following: payload.following,
          slug: payload.slug,
          followSlug: payload.followSlug,
          preferences: payload.preferences,
        });
      } else {
        console.log("Unknown follow type: " + payload.type);
        console.log(payload);
      }
    },
    CHANGE_FOLLOW_OVERVIEW_FETCHING(state, payload) {
      state.followOverviewFetching = payload;
      
    },
    CHANGE_FOLLOW_OVERVIEW(state, payload) {
      console.log(payload);

      if (state.followOverview[payload.tab]) {
        Vue.set(state.followOverview, payload.tab, payload.items);
        Vue.set(state.followOverview, "userSlug", payload.userSlug);
      } else {
        console.log("Unknown follow tab: " + payload.type);
        console.log(payload);
      }
    },
    CHANGE_NOTIFIATION_PREFERENCE(state, payload) {
      if (state.followMemory[payload.type][payload.followSlug]['preferences']) {
        Vue.set(state.followMemory[payload.type][payload.followSlug]['preferences'], 'notification', payload.notification);
      }
    },
    CHANGE_EMAIL_PREFERENCES(state, payload) {
      if (state.followMemory[payload.type][payload.followSlug]['preferences']) {
        Vue.set(state.followMemory[payload.type][payload.followSlug]['preferences'], 'emails', payload.list);
      }
    }
 
  },
  actions: {
    CLEAR_WHO_I_FOLLOW({ commit }) {
        commit('CLEAR_FOLLOW_MEMORY')
    },
    NO_SUBWIKIS_FOLLOWED({dispatch}){
      dispatch("SubWikiStore/NO_SUBWIKIS_FOLLOWED", null, { root: true });
    },
    SET_SUBWIKI_BATCH_DATA({dispatch}, payload){
      let subwikisToGet = []
      let i 
      for(i in payload){
        subwikisToGet.push(payload[i]['followSlug'])
      }
      let subwikiBatchToGet = {
        'batchtoget': subwikisToGet,
        'type': 'FollowingSubwikis'
      }
      dispatch("SubWikiStore/FETCH_SUBWIKI_BATCH", subwikiBatchToGet, { root: true });
    },
    async FETCH_WHO_I_FOLLOW({ commit, getters, rootGetters, state, dispatch}) {
      // FOLLOW_MEMORY is used for both browsing everything (so it keeps what you are and are not following)
      // AND ALSO for browsing things that you already follow
      // So the best way to accomodate both is just to clear the follow memory
      // Otherwise it doesn't accurately reflect who you follow, it's just a big ol' list of every follow
      // relation, whether true or false
      commit('CLEAR_FOLLOW_MEMORY')
      /////////////
      console.log("WE HIT FOLLOW MEMORY")
 
      var url = `${process.env.VUE_APP_APIBASEURL}relfollow/following-users/${rootGetters["UserStore/userProfile"].slug}`;
      console.log(url);
      await axios
      .get(encodeURI(url), rootGetters["UserStore/authHeader"])
      .then(function (response) {
          console.log(response);
          if (Object.keys(response.data).length === 0){
            dispatch('NO_SUBWIKIS_FOLLOWED')
          }
          else{
            let i;
            for (i in response.data) {
 
            commit("CHANGE_FOLLOW_MEMORY", {
              slug: getters['FOLLOW_SLUG']({slug: response.data[i].parentSlug, type: response.data[i].followType}),          
              type: response.data[i].followType,
              followLabel: response.data[i].followLabel,
              followSlug: response.data[i].parentSlug,
              following: true,
              preferences: response.data[i]?.preferences ? response.data.preferences : state.DefaultPreferences
            });
          }
          dispatch('SET_SUBWIKI_BATCH_DATA', state.followMemory['subwiki'])}
        })
        .catch(function (error) {
          console.log("Follow response:");
          console.log(error);

        });
    },
    async FETCH_USERS_FOLLOWS({ commit, rootGetters}, payload) {

      var url;
      commit('CHANGE_FOLLOW_OVERVIEW_FETCHING', true);
      url =
      process.env.VUE_APP_APIBASEURL +
      "relfollow/"+ payload.tab +'/'+payload.userSlug
      ;
      await axios
      .get(encodeURI(url), rootGetters["UserStore/authHeader"])
      .then(function (response) {
        commit("CHANGE_FOLLOW_OVERVIEW", {
          userSlug: payload.userSlug,
          tab: payload.tab,
          items: response.data
        });
        commit('CHANGE_FOLLOW_OVERVIEW_FETCHING', false);
        
      })
      .catch(function (error) {
        commit('CHANGE_FOLLOW_OVERVIEW_FETCHING', false);
        console.log("Follow response:");
        console.log(error);

        });
    },
    FETCH_FOLLOW_STATUS({ commit, getters, rootGetters, state }, payload) {

      if (payload && payload.type && payload.slug) {
        var url;
        url =
          process.env.VUE_APP_APIBASEURL +
          "relfollow/get/" +
          payload.type +
          "/" +
          payload?.slug.trim();

        axios
          .get(encodeURI(url), rootGetters["UserStore/authHeader"])
          .then(function (response) {

            commit("CHANGE_FOLLOW_MEMORY", {
              key: { pk: response.data.pk, sk: response.data.sk},
              slug: getters['FOLLOW_SLUG'](payload),
              type: payload.type,
              followLabel: payload.name,
              followSlug: payload.slug,
              following: response.data?.followType ? true : false,
              preferences: response.data?.preferences ? response.data.preferences : state.DefaultPreferences
            });
          })
          .catch(function (error) {
            console.log("Follow response:");
            console.log(error);

          });
      }
    },
    CREATE_FOLLOW_RELATIONSHIP({ commit, rootGetters, getters, state}, payload) {
      var url = process.env.VUE_APP_APIBASEURL + "relfollow";
      var putdata = {
        parent: { pk: payload.parentSK, sk: payload.parentSK },
        followType: payload.type,
        parentSlug: payload.slug,
        preferences: state.DefaultPreferences
      };
      commit("CHANGE_FOLLOW_MEMORY", {
        slug: getters['FOLLOW_SLUG'](payload),          
        type: payload.type,
        key: { pk: payload.pk, sk: payload.sk},
        followLabel: payload.name,
        followSlug: payload.slug,
        following: true,
        preferences: state.DefaultPreferences
      });
      axios
        .post(url, putdata, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if (typeof response.data.pk == "undefined") {            
            console.log("No id");
          }
        })
        .catch(function (error) {
          console.log(error)
        });
    },
    DELETE_FOLLOW_RELATIONSHIP({ commit, rootGetters }, payload) {

      let slug = payload.type + "/" + payload.slug.trim();
      var url = process.env.VUE_APP_APIBASEURL + "relfollow/delete/" + slug;
      commit("CHANGE_FOLLOW_MEMORY", {
        slug: slug,
        type: payload.type,
        followSlug: payload.slug,
        following: false,
        
      });
      axios
        .delete(url, rootGetters["UserStore/authHeader"])
        .then(function () {
        })
        .catch(function (error) {
          console.log(error)
        });
    },
    UPDATE_FOLLOW_NOTIFIATION_PREFERENCES({ commit }, payload) {
      commit('CHANGE_NOTIFIATION_PREFERENCE', payload);
    },
    UPDATE_FOLLOW_EMAIL_PREFERENCES({ commit }, payload) {
      commit('CHANGE_EMAIL_PREFERENCES', payload);
    },
    SAVE_FOLLOW_EMAIL_CHOICES({  rootGetters }, payload) {
      
        var url = process.env.VUE_APP_APIBASEURL + "relfollow";
      
        var putdata = {
          key:  payload.key,
          preferences: payload.preferences
        };
        axios
          .put(url, putdata, rootGetters["UserStore/authHeader"])
          .then(function (response) {
            if (typeof response.data.pk == "undefined") {            
              console.log("No id");
            }
          })
          .catch(function (error) {
            console.log(error)
          });
      
    }
  },
};

