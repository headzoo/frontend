import axios from "axios";
import i18n from "@/plugins/i18n";

export default {
  namespaced: true,
  state: {
    OldText: "",
    NewText: "",
    OldCardUrl: "",
    NewCardUrl: "",
    Debug: [],
    EditKey: { pk: null, sk: null },
    EditCommentsFor: "",
    CurrentContext: "",
    ShowDialog: false,
    LastSave: null,
    IsMine: null,
    Contexts: {
      comment: {
        button: i18n.t("General.comment"),
        label: i18n.t("General.comment-text"),
        update: "commentText",
      },
      post: {
        button: i18n.t("General.post"),
        label: i18n.t("General.post-text"),
        update: "postText",
      },
    },
  },
  getters: {
    GET_IS_MINE(state){
      return state.IsMine
    },
    GET_OLD_TEXT: (state) => {
      return state.OldText;
    },
    GET_NEW_TEXT: (state) => {
      return state.NewText;
    },
    GET_OLD_CARD_URL: (state) => {
      return state.OldCardUrl;
    },
    GET_NEW_CARD_URL: (state) => {
      return state.NewCardUrl;
    },
    GET_CONTEXTS: (state) => {
      return state.Contexts;
    },
    GET_CURRENT_CONTEXT: (state) => {
      return state.CurrentContext;
    },
    GET_EDIT_KEY: (state) => {
      return state.EditKey;
    },
    GET_SHOW_DIALOG: (state) => {
      return state.ShowDialog;
    },
    GET_COMMENTSFOR: (state) => {
      return state.EditCommentsFor;
    },
    GET_LAST_SAVE: (state) => {
      return state.LastSave;
    },
  },
  mutations: {
    SET_IS_MINE(state, payload){
      state.IsMine = payload
    },
    ADD_DEBUG(state, payload) {
      state.Debug.push(payload);
    },
    SET_OLD_TEXT(state, payload) {
      state.OldText = payload;
    },
    SET_NEW_TEXT(state, payload) {
      state.NewText = payload;
    },
    SET_OLD_CARD_URL(state, payload) {
      state.OldCardUrl = payload;
    },
    SET_NEW_CARD_URL(state, payload) {
      console.log('set new card url')
      state.NewCardUrl = payload;
    },
    SET_CURRENT_CONTEXT(state, payload) {
      state.CurrentContext = payload;
    },
    SET_EDIT_KEY(state, payload) {
      state.EditKey = payload;
    },
    SET_SHOW_DIALOGUE(state, payload) {
      state.ShowDialog = payload;
    },
    SET_EDIT_COMMENTSFOR(state, payload) {
      state.EditCommentsFor = payload;
    },
    SET_LAST_SAVE(state, payload) {
      state.LastSave = payload;
    },
  },
  actions: {
    SET_IS_MINE({commit}, payload){
      commit("SET_IS_MINE", payload)
    },
    UPDATE_EDIT_COMMENTSFOR({ commit }, payload) {
      commit("SET_EDIT_COMMENTSFOR", payload);
    },
    UPDATE_OLD_TEXT({ commit }, payload) {
      commit("SET_OLD_TEXT", payload);
    },
    UPDATE_NEW_TEXT({ commit }, payload) {
      commit("SET_NEW_TEXT", payload);
    },
    UPDATE_OLD_CARD_URL({ commit }, payload) {
      commit("SET_OLD_CARD_URL", payload);
    },
    UPDATE_NEW_CARD_URL({ commit }, payload) {
      commit("SET_NEW_CARD_URL", payload);
    },
    UPDATE_CURRENT_CONTEXT({ commit }, payload) {
      commit("SET_CURRENT_CONTEXT", payload);
    },
    UPDATE_EDIT_KEY({ commit }, payload) {
      commit("SET_EDIT_KEY", payload);
    },
    UPDATE_SHOW_DIALOGUE({ commit }, payload) {
      commit("SET_SHOW_DIALOGUE", payload);
    },
    SAVEEDIT_POST({ commit, state, dispatch, rootGetters }) {
      var url = ""
      if(state.IsMine == false){
        url =
        process.env.VUE_APP_APIBASEURL + state.CurrentContext + "/update";
      }
      else{
        url =
        process.env.VUE_APP_APIBASEURL + state.CurrentContext + "/updateself";
      }

      let update = {
        key: state.EditKey,
        [state.Contexts[state.CurrentContext].update]: state.NewText,
        cardUrl: state.NewCardUrl
      };
      axios
        .put(url, update, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if (typeof response.data != "undefined") {
            commit("SET_SHOW_DIALOGUE", false);
            if (state.CurrentContext == "post") {
              dispatch(
                "PostStore/UPDATE_POST_READING_TEXT",
                state.NewText,
                {
                  root: true,
                }
              );
              dispatch(
                "PostStore/UPDATE_POST_ITEM",
                {
                  slug: state.EditKey.slug,
                  newText: state.NewText,
                  cardUrl: state.NewCardUrl,
                },
                {
                  root: true,
                }
              );
              dispatch(
                "InboxStore/UPDATE_FEED_ITEM",
                {
                  slug: state.EditKey.slug,
                  newText: state.NewText,
                  cardUrl: state.NewCardUrl,
                },
                {
                  root: true,
                }
              );
            } else if (state.CurrentContext == "comment") {
              dispatch(
                "CommentStore/UPDATE_COMMENT_ITEM",
                {
                  commentOn: 'commentOn-'+state.EditKey.pk,
                  slug: state.EditKey.slug,
                  newText: state.NewText,
                },
                {
                  root: true,
                }
              );
            }
            commit("SET_LAST_SAVE", new Date());
          } else {
            console.log("No id");
          }
          commit("ADD_DEBUG", response);
        })
        .catch(function (error) {
          commit("ADD_DEBUG", error);
        });
    },
  },
};
