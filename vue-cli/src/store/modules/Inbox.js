import axios from "axios";
export default {
  namespaced: true,
  state: {
    NewPosts: [],
    FeedFetched: false,
    NotificationsFetched: false,
    UnreadNotifications: 0 ,
    FeedInbox: [],
    NotificationInbox: [],
    EndOfList: false,
    LastEvaluatedKey: null
  },
  getters: {
    GET_NEW_POSTS: (state) => {
      return state.NewPosts;
    },
    GET_FEED: (state) => {
      return state.FeedInbox;
    },
    GET_FEED_FETCHED: (state) => {
      return state.FeedFetched;
    },
    GET_LAST_EVALUATED_KEY: (state) => {
      return state.LastEvaluatedKey
    },
    GET_UNREAD_COUNT: (state) => {
      return state.UnreadNotifications;
    },
    GET_NOTIFICATIONS: (state) => {
      return state.NotificationInbox;
    },
    GET_NOTIFICATIONS_FETCHED: (state) => {
      return state.NotificationsFetched;
    },
    GET_END_OF_FEED: (state) => {
      return state.EndOfList;
    },
  },
  mutations: {
    RESET_END_OF_LIST(state) {
      state.EndOfList = false;
    },
    SET_END_OF_LIST(state) {
      state.EndOfList = true;
    },
    PUSH_TO_FEED(state, payload) {
      let i;
      for (i in payload) {
        state.FeedInbox.push(payload[i]);
      }
    },
    SET_FEED_ITEM(state, payload) {
      state.FeedInbox[payload.index].postText = payload.newText;
      state.FeedInbox[payload.index].cardUrl = payload.cardUrl;
    },
    SET_FEED_FETCHED(state, payload) {
      state.FeedFetched = payload ? true : false;
    },
    SET_NOTIFICATIONS_FETCHED(state, payload) {
      state.NotificationsFetched = payload ? true : false;
    },
    RESET_NOTIFICATIONS_READ(state) {
      state.UnreadNotifications = 0;
      let f;
      let notifications = [];
      for (f in state.NotificationInbox) {
        let n = state.NotificationInbox[f];
        n.item.read = true;
        notifications.push(n);        
      }
      state.NotificationInbox = notifications;
    },
    SET_FEED(state, payload) {
      let f;
      let feed = [];
      for (f in payload) {
        feed.push(payload[f]);
      }
      state.FeedInbox = feed;
    },
    ADD_NEW_POSTS(state, payload) {      
      state.NewPosts.push(payload);
    },
    ADD_NEW_POSTS_TO_FEED(state) {
      let i;
      let f;
      let feed = [];
      for (i in state.NewPosts) {
        feed.push(state.NewPosts[i])
      }
      for (f in state.FeedInbox) {
        feed.push(state.FeedInbox[f]);
      }
      state.FeedInbox = feed;
      state.NewPosts = [];
    },
    SET_NOTIFICATIONS(state, payload) {
      let f;
      let notifications = [];
      let unread = 0;
      for (f in payload) {
        notifications.push(payload[f]);
        if (payload[f].item.read == false) {
          unread++;
        }
      }
      state.UnreadNotifications = unread;
      state.NotificationInbox = notifications;
    },
    PREPEND_FEED_LIST(state, payload) {
      state.FeedInbox.unshift(payload);
    },
    PREPEND_NOTIFICATION_LIST(state, payload) {
      state.NotificationInbox.unshift(payload);
      let unread = 0;
      let notifications = [];
      let f;
      for (f in state.NotificationInbox) {
        notifications.push(payload[f]);
        if (state.NotificationInbox[f].item.read == false) {
          unread++;
        }
      }
      state.UnreadNotifications = unread;
    },
    SET_LAST_EVALUATED_KEY(state, payload){
      state.LastEvaluatedKey = payload
    }
  },
  actions: {
    
    UPDATE_FEED_ITEM({state, commit }, payload) {
      let i = 0;
      let updateIndex;
      for (i in state.FeedInbox) {
        if (state.FeedInbox[i].slug == payload.slug) {
          
          updateIndex = i;
          break;
        }
      }
      if (updateIndex) {
        commit("SET_FEED_ITEM", {
          index: updateIndex,
          newText: payload.newText,
          cardUrl: payload.cardUrl
        });
      }
      // commit("SET_LIST", newList);
    },
    SERVER_SEND_FEED_UPDATE({ state, commit }, payload) {
      let i;
      for (i in state.FeedInbox) {
        if (state.FeedInbox[i].slug == payload.slug) {
          
          return false;
        }
      }
      commit("ADD_NEW_POSTS",  payload );

    },
    LOAD_NEW_POSTS_INTO_FEED({ commit }) {
      commit('ADD_NEW_POSTS_TO_FEED');
    },
    SERVER_SEND_NOTIFICATION({commit }, payload) {
      commit("PREPEND_NOTIFICATION_LIST", { item: payload });

    },
    MARK_UNREAD_AS_READ({commit,  state, dispatch }) {
      if (state.UnreadNotifications > 0) {
        commit('RESET_NOTIFICATIONS_READ');
        dispatch('SocketStore/SOCKET_SEND_MESSAGE', {
          action: 'CLIENT_MARK_NOTIFICATION_READ',
          payload: {

          }
        }, {
          root: true,
        });
      }
    },
    FETCH_FEED({ commit, state, rootGetters }) {
      if (!rootGetters["UserStore/isLoggedIn"]) {
        return false;
      }
      var url;
      url = process.env.VUE_APP_MEGAPHONEBASEURL + "inbox/feed";
      if (state.LastEvaluatedKey != null){
        url += "?start=" + encodeURIComponent(state.LastEvaluatedKey.sk)
      }
      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log("///////////////////////////////////")
          console.log(response.data.LastEvaluatedKey)
          console.log("///////////////////////////////////")
          if (typeof response.data.Items != "undefined") {
            commit("SET_FEED_FETCHED", true);
            if (state.LastEvaluatedKey != null) {
              commit("PUSH_TO_FEED", response.data.Items);
            } else {
              commit("SET_FEED", response.data.Items);
            }
            if(response.data.LastEvaluatedKey){
              commit("SET_LAST_EVALUATED_KEY", response.data.LastEvaluatedKey)
            }
            else{
              commit("SET_END_OF_LIST");
            }
          } else {
            if (state.LastEvaluatedKey == null) {
              commit("SET_FEED", {});
            } else {
              commit("SET_END_OF_LIST");
            }
          }
          
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    FETCH_NOTIFICATIONS({ commit, rootGetters }, payload = null) {
      var url;

      url = process.env.VUE_APP_MEGAPHONEBASEURL + "inbox/notifications";
      if (typeof payload == "string") {
        url += "?since=" + payload;
      }
      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_NOTIFICATIONS_FETCHED", true);
          commit("SET_NOTIFICATIONS", response.data.Items);
          
        })
        .catch(function (error) {
          console.log(error.toJSON());
          // dispatch('UserStore/checkIfMustLogin', error, {root: true});
          
        });
    },
    SET_LAST_EVALUATED_KEY({commit}, payload){
      commit("SET_LAST_EVALUATED_KEY", payload)
    },
  },
};
