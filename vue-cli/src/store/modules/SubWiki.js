import axios from "axios";
import router from "../../router/index";

export default {
    namespaced: true,
    state: {
        Debug: [],
        
        CurrentSubwiki: {
            subwikiLabel: "",
            subwikiLang: "",
            subwikiDesc: "",
        },
        NewSubwiki: {},
        SubWikiLists: {
            "AllSubwikis": [],
            "FollowingSubwikis": [],
            "OnboardingSubwikis": []
        },
        UIFeedbackMsg: "",
        UpdateState: "idle",
        HasLoaded: [],
        NoSubwikisFollowed: false
    },
    getters: {
        GET_ONBOARDING_SUBWIKIS: (state) =>{
            return state.SubWikiLists['OnboardingSubwikis']
        },
        GET_CURRENT_SUBWIKI: (state) => {
            return state.CurrentSubwiki;
        },
        GET_CURRENT_SUBWIKI_DESC: (state) => {
            return state.CurrentSubwiki.subwikiDesc;
        },
        GET_POST_DEBUG: (state) => {
            return state.Debug;
        },
        GET_SUBWIKI_LIST: (state) => {
            return state.SubWikiLists['AllSubwikis'];
        },
        GET_FOLLOWING_SUBWIKI_LIST: (state) => {
            return state.SubWikiLists['FollowingSubwikis']
        },
        GET_NEW_SUBWIKI: (state) => {
            return state.NewSubwiki;
        },
        GET_NEW_SUBWIKI_LANG: (state) => {
            return state.NewSubwiki;
        },
        GET_NEW_SUBWIKI_FEEDBACK: (state) => {
            return state.UIFeedbackMsg;
        },
        GET_NEW_SUBWIKI_LABEL: (state) => {
            return state.NewSubwiki.subwikiLabel;
        },
        GET_NEW_SUWIKI_DESC: (state) => {
            return state.NewSubwiki.subwikiDesc;
        },
        GET_UPDATE_STATE: (state) => {
            return state.UpdateState;
        },
        GET_HAS_SUBWIKI_LOADED: (state) => {
            return state.HasLoaded;
        },
        GET_NO_SUBWIKIS_FOLLOWED: (state) => {
            return state.NoSubwikisFollowed
        }
    },
    mutations: {
        SET_SUBWIKI_CARD_DETAILS(state, payload){
            if(state.paginateCurrentSubwikiDetails[payload.type].some(subwiki => subwiki.pk === payload.data.pk)){
                console.log("Subwiki card already loaded");
            } else{
                state.paginateCurrentSubwikiDetails[payload.type].push(payload.data)
            }
        },
        SET_CURRENT_SUBWIKI(state, payload) {
            state.CurrentSubwiki = payload;
            state.HasLoaded.push(payload.slug);
        },
        ADD_DEBUG(state, payload) {
            state.Debug.push(payload);
        },
        SET_SUBWIKI_LIST(state, payload) {
            state.SubWikiLists['AllSubwikis'] = payload
        },
        SET_SUBWIKI_BATCH_LIST(state, payload){
            state.SubWikiLists[payload['type']] = []
            state.SubWikiLists[payload['type']] = payload['subwikis']
            console.log(state.SubWikiLists[payload['type']])
        },
        SET_UI_FEEDBACK(state, payload) {
            state.UIFeedbackMsg = payload;
        },
        RESET_NEW_SUBWIKI(state, payload) {
            state.NewSubwiki = {
                subwikiLabel: "",
                subwikiLang: payload,
                subwikiDesc: "",
            };
            state.UIFeedbackMsg = "";
        },
        SET_NEW_SUBWIKI_LABEL(state, payload) {
            state.NewSubwiki.subwikiLabel = payload;
        },
        SET_NEW_SUBWIKI_DESC(state, payload) {
            state.NewSubwiki.subwikiDesc = payload;
        },
        SET_NEW_SUBWIKI_LANG(state, payload) {
            state.NewSubwiki.subwikiLang = payload;
        },
        SET_NEW_SUBWIKI_SLUG(state, payload) {
            state.NewSubwiki.slug = payload;
        },
        SET_SUBWIKI_DESC(state, payload) {
            state.CurrentSubwiki.subwikiDesc = payload;
        },
        SET_UPDATE_STATE(state, payload) {
            state.UpdateState = payload;
        },
        NO_SUBWIKIS_FOLLOWED(state){
            state.NoSubwikisFollowed = true
        }
    },
    actions: {
        NO_SUBWIKIS_FOLLOWED({commit}){
            commit("NO_SUBWIKIS_FOLLOWED")
        },
        SET_FOLLOW_SUBWIKI_LIST({commit}, payload){
            commit("SET_FOLLOW_SUBWIKI_LIST", payload)
        },
        UPDATE_SUBWIKI_LIST({ commit }, payload) {
            commit("SET_SUBWIKI_LIST", payload);
        },
        RESET_NEW_SUBWIKI({ commit, rootGetters }) {
            commit("RESET_NEW_SUBWIKI", rootGetters["UserStore/userLanguage"]);
        },
        UPDATE_NEW_SUBWIKI_LABEL({ commit }, payload) {
            commit("SET_NEW_SUBWIKI_LABEL", payload);
        },
        UPDATE_NEW_SUBWIKI_DESC({ commit }, payload) {
            commit("SET_NEW_SUBWIKI_DESC", payload);
        },
        UPDATE_NEW_SUBWIKI_LANG({ commit }, payload) {
            commit("SET_NEW_SUBWIKI_LANG", payload);
        },
        UPDATE_SUBWIKI_DESC({ commit }, payload) {
            commit("SET_SUBWIKI_DESC", payload);
        },
        SAVE_SUBWIKI_DESC({ state, commit, rootGetters }) {
            var url = `${process.env.VUE_APP_APIBASEURL}subwiki/${state.CurrentSubwiki.slug}`;
            let update = {
                key: { pk: state.CurrentSubwiki.pk, sk: state.CurrentSubwiki.sk },
                subwikiDesc: state.CurrentSubwiki.subwikiDesc,
            };
            commit("SET_UPDATE_STATE", "updating");
            axios
                .put(url, update, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (typeof response.data.Attributes.subwikiDesc != "undefined") {
                        commit("SET_UPDATE_STATE", "completed");
                    } else {
                        console.log("No id");
                    }
                    commit("ADD_DEBUG", response);
                })
                .catch(function(error) {
                    commit("SET_UPDATE_STATE", "error");
                    commit("ADD_DEBUG", error);
                });
        },
        CREATE_NEW_SUBWIKI({ commit, state, rootGetters, dispatch }) {
            var url = process.env.VUE_APP_APIBASEURL + "subwiki";
            axios
                .post(url, state.NewSubwiki, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (typeof response.data.slug != "undefined") {
                        
                        commit("SET_NEW_SUBWIKI_SLUG", response.data.slug);
                        commit("SET_UI_FEEDBACK", "Created!");

                        dispatch('FollowStore/CREATE_FOLLOW_RELATIONSHIP', {type: response.data.entity, parentSK: response.data.pk, slug: response.data.slug}, {root: true})
                        
                                            
                    } else {
                        commit("SET_UI_FEEDBACK", "No slug!");
                        console.log("No slug");
                    }
                    commit("ADD_DEBUG", response);
                })
                .catch(function(error) {
                    commit("ADD_DEBUG", error);
                    commit("SET_UI_FEEDBACK", "Error!");
                });
                // 

        },
        async FETCH_SUBWIKI_LIST({ commit, rootGetters }) {
            console.log("We're fetching subwikis")
            var url;
            url = process.env.VUE_APP_APIBASEURL + "subwiki";

            await axios
                .get(url, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (response.data) {
                        let list = new Array()
                        let i = 0;
                        for (i in response.data) {
                            list.push({
                                subwikiLabel: response.data[i].subwikiLabel,
                                slug: response.data[i].slug,
                                subwikiDesc: response.data[i].subwikiDescription,
                                subwikiLang: response.data[i].subwikiLang,
                                statistics: response.data[i].statistics,
                                popularity: parseInt(response.data[i].statistics?.totalSubwikiPosts) * parseInt(response.data[i].statistics?.totalFollowers),
                            });
                        }

                        commit("SET_SUBWIKI_LIST", list);
                    } else {
                        commit("SET_SUBWIKI_LIST", []);
                        commit("ADD_DEBUG", "Empty post list");
                    }
                })
                .catch(function(error) {
                    commit("ADD_DEBUG", error);
                });
        },
        SET_CURRENT_BROWSE_SUBWIKIS({commit}, payload){
            commit("SET_CURRENT_BROWSE_SUBWIKIS", payload)
        },
        async SEARCH_SUBWIKIS({ commit, rootGetters }, payload) {
            var url;
            console.log(payload)
            url = process.env.VUE_APP_APIBASEURL + "subwiki/search/"+payload;
            console.log(url)
            commit("SET_SUBWIKI_LIST", []);
            return axios
                .get(url, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (response.data.Items) {
                        let list = [];
                        let i = 0;
                        for (i in response.data.Items) {
                            list.push({
                                subwikiLabel: response.data.Items[i].subwikiLabel,
                                slug: response.data.Items[i].slug,
                                subwikiDesc: response.data.Items[i].subwikiDescription,
                                subwikiLang: response.data.Items[i].subwikiLang,
                                statistics: response.data.Items[i].statistics,
                            });
                        }
                        commit("SET_SUBWIKI_LIST", list);
                    } else {
                        commit("SET_SUBWIKI_LIST", []);
                        commit("ADD_DEBUG", "Empty post list");
                    }
                    return true;
                })
                .catch(function(error) {
                    commit("ADD_DEBUG", error);
                });
        },
        FETCH_SUBWIKI_BATCH({commit, rootGetters, state}, payload){
            state.NoSubwikisFollowed = false
            var url
            url = process.env.VUE_APP_APIBASEURL + "subwiki/batch";
            let getbatchpayload = {
                slugs: payload['batchtoget']
            }
            axios
                .post(url, getbatchpayload, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (typeof response.data != "undefined") {
                            let commitpayload = {
                                'type': payload['type'],
                                'subwikis': response.data
                            }
                        console.log("Well that worked")
                        commit("SET_SUBWIKI_BATCH_LIST", commitpayload)
                    }
                    else{
                        console.log("Well that didn't work");
                        commit("ADD_DEBUG", "Failed to fetch");
                    }})
        },
        async FETCH_SUBWIKI_DETAILS({ commit, rootGetters }, payload) {
            var url;
            url = process.env.VUE_APP_APIBASEURL + "subwiki/" + payload;
            await axios
                .get(url, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (typeof response.data != "undefined") {
                        commit("SET_CURRENT_SUBWIKI", response.data);
                        // dispatch("PostStore/UPDATE_NEW_POST_SUBWIKI", response.data, {root: true});
                    } else {
                        commit("SET_CURRENT_SUBWIKI", {});
                        commit("ADD_DEBUG", "Empty subwiki");
                    }
                })
                .catch(function(error) {
                    if (error.response.status === 404){
                        console.log(error.response.status)
                        console.log("error.response.status")
                            router.push({
                                name: "404 - Page not 'here'", params: {LANG: payload}
                              })
                    }
                    commit("ADD_DEBUG", error);
                });
        },
        FETCH_SUBWIKI_CARD_DETAIL({commit, rootGetters}, payload) {
            // console.log("We're fetching card details")
            // console.log(payload.tosend)
            var url;
            url = process.env.VUE_APP_APIBASEURL + "subwiki/" + payload.tosend;
            axios
                .get(url, rootGetters["UserStore/authHeader"])
                .then(function(response) {
                    if (typeof response.data != "undefined") {
                        let tocommit ={
                            data: response.data,
                            type: payload.type
                        }
                        commit("SET_SUBWIKI_CARD_DETAILS", tocommit);
                    } else {
                        commit("ADD_DEBUG", "Empty subwiki");
                    }
                })
                .catch(function(error) {
                    commit("ADD_DEBUG", error);
                });
        }
    },
};

