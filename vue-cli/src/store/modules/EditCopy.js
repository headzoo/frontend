import axios from "axios";

export default {
  namespaced: true,
  state: {
    updatedText: "",
    editLanguage: "",
    aboutus: "",
    copytoedit: {},
    languages: ['English', 'Español', 'Deutsch', 'Português', 'Русский'],
    stringstoedit: {},
    objectstoedit: {},
    expandedobjects: []
  },
  actions: {
    UPDATE_TEXT({commit}, payload){
        commit("UPDATE_TEXT", payload) 
    },
    SORT_STRINGS({commit}, payload){
      commit("SORT_STRINGS", payload)
    },
    IS_OBJECT_EXPANDED({commit}, payload){
      commit("IS_OBJECT_EXPANDED", payload)
    },
  // UPDATE COPY IS MY BEST FRIEND NOW
  UPDATE_COPY({ commit, rootGetters } , payload) {
    var url = process.env.VUE_APP_APIBASEURL + "copy"
    var putdata = {
      key: rootGetters["EditCopy/GET_CURRENT_LANGUAGE"],
      toUpdate: payload
    };
    axios
    .post(url, putdata, rootGetters["UserStore/authHeader"])
    .then(function () {
  
    })
    // DON'T HAVE ANYTHING TO COMMIT AT THE MOMENT THIS IS NOT IN USE
    commit("COMMIT_ABOUT_US_EDIT", payload);
  },
  GET_SELECTED_COPY_TO_EDIT({ commit, rootGetters }, payload){
    var url = process.env.VUE_APP_APIBASEURL + "getcopy"
    var retrievaldata = {
      key: payload,
    };
    axios
    .post(url, retrievaldata, rootGetters["UserStore/authHeader"])
    .then(function (response) {
      commit("GET_SELECTED_COPY_TO_EDIT", response);
    })
    commit("SET_CURRENT_LANGUAGE", payload)
  },
},
  mutations: {
    SET_CURRENT_LANGUAGE(state, payload){
      state.editLanguage = payload
    },
    UPDATE_TEXT(state, payload) {
      state.aboutus = payload;
    },
    // This is redundant at this point
    COMMIT_ABOUT_US_EDIT(state, payload) {
      state.updatedText = payload.text
      state.editLanguage = payload.language
  },
    GET_SELECTED_COPY_TO_EDIT(state, payload){
      state.copytoedit = payload["data"][0]
      state.stringstoedit = payload["data"][1]
      state.objectstoedit = payload["data"][2]
      },
    IS_OBJECT_EXPANDED(state, payload){
        if(state.expandedobjects.includes(payload)){
          let indexofexpandedvalue = state.expandedobjects.indexOf(payload)
          state.expandedobjects.splice(indexofexpandedvalue, 1)
        }
        else{
          state.expandedobjects.push(payload)
        }
      }
},
getters: {
  GET_ABOUT_US(state) {
    return state.aboutus
},
  GET_LANGUAGES(state) {
    return state.languages
  },
  GET_COPY_TO_EDIT(state){
    return state.copytoedit
  },
  GET_STRINGS_TO_EDIT(state){
    return state.stringstoedit
  },
  GET_OBJECTS_TO_EDIT(state){
    return state.objectstoedit
  },
  GET_CURRENT_LANGUAGE(state){
    return state.editLanguage
  },
  GET_EXPANDED_OBJECTS(state){
    return state.expandedobjects
  }
}}