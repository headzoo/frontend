import axios from "axios";

export default {
  namespaced: true,
  state: {
    blockedUsers: [],
    showBlockDialog: false,
    showUnblockDialog: false,
    showCannotBlockAdminDialog: false,
    userToBlock: null,
    reasonForBlock: null,
    revealedContent: {
      comment: [],
      post: [],
    },
  },
  getters: {
    IS_USER_BLOCKED: (state) => (slug) => {
      return (
        state.blockedUsers.filter(
          (blockeduser) => slug === blockeduser.data.userProfile.slug
        ).length > 0
      );
    },
    IS_REVEALED: (state) => (payload) => {
      if (
        !payload?.["entity"] ||
        !payload?.["slug"] ||
        typeof state.revealedContent?.[payload["entity"]] === "undefined"
      ) {
        return false;
      }
      return state.revealedContent[payload["entity"]].includes(payload["slug"]);
    },
    GET_BLOCKED_USERS(state) {
      return state.blockedUsers;
    },
    GET_SHOW_DIALOG(state) {
      return state.showBlockDialog;
    },
    GET_SHOW_UNBLOCK_DIALOG(state) {
      return state.showUnblockDialog;
    },
    BLOCK_GET_USER(state) {
      return state.userToBlock;
    },
    GET_REASON_FOR_BLOCK(state) {
      return state.reasonForBlock;
    },
    GET_CANNOT_BLOCK_ADMIN_DIALOG(state) {
      return state.showCannotBlockAdminDialog;
    },
  },
  mutations: {
    SET_BLOCKED_USERS(state, payload) {
      state.blockedUsers = payload;
    },
    ADD_BLOCKED_USER(state) {
      state.blockedUsers.push({
        data: {
          userProfile: state.userToBlock,
        },
        entity: "userblock"
      });
    },
    UNDO_ADD_BLOCKED_USER(state) {
      state.blockedUsers.pop();
    },
    APPEND_BLOCKED_USERS(state, payload) {
      state.blockedUsers.push(payload);
    },
    APPEND_REVEALED_CONTENT(state, payload) {
      state.revealedContent[payload["entity"]].push(payload["slug"]);
    },
    SET_SHOW_DIALOG(state, payload) {
      state.showBlockDialog = payload;
    },
    SET_SHOW_UNBLOCK_DIALOG(state, payload) {
      state.showUnblockDialog = payload;
    },
    SET_USER_TO_BLOCK(state, payload) {
      state.userToBlock = payload;
    },
    SET_REASON_TO_BLOCK(state, payload) {
      state.reasonForBlock = payload;
    },
    REMOVE_BLOCKED_USER(state, payload) {
      state.blockedUsers = state.blockedUsers.filter(
        (item) => item.data.userProfile.userSlug !== payload
      );
    },
    SET_ADMIN_BLOCK_DIALOG(state, payload) {
      state.showCannotBlockAdminDialog = payload;
    }
  },
  actions: {
    CLEAR_WHO_I_BLOCKED({ commit }) {
      commit("SET_BLOCKED_USERS", []);
    },
    GET_BLOCKED_USERS({ commit, rootGetters }) {
      var url = "";
      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_BLOCKED_USERS", response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async FETCH_BLOCKED_USERS({ commit, rootGetters }) {
      var url = process.env.VUE_APP_APIBASEURL + "userblocks";
      await axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_BLOCKED_USERS", response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async SAVE_AND_CLOSE({ state, commit, rootGetters, dispatch }) {
      var url = process.env.VUE_APP_APIBASEURL + "userblocks";

      let blockDetails = {
        userID: state.userToBlock.userID,
        userSlug: state.userToBlock.slug,
        reason: state.reasonForBlock,
      };
      commit("ADD_BLOCKED_USER");
      await axios
        .post(url, blockDetails, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);

          dispatch("CLOSE_BLOCK_DIALOG");
        })
        .catch(function (error) {
          if (error.response.data?.message == 'You cannot block an admin') {
            commit("SET_ADMIN_BLOCK_DIALOG", true);
            commit("UNDO_ADD_BLOCKED_USER");
          } else {
            console.log(error);
          }

        });
    },
    UPDATE_ADMIN_BLOCK_DIALOG({ commit }, payload= false) {
      commit("SET_ADMIN_BLOCK_DIALOG", payload);
    },
    async REMOVE_BLOCKED_USER({ commit, rootGetters, dispatch }, payload) {
      var url = process.env.VUE_APP_APIBASEURL + "userblocks/" + payload;
      await axios
        .delete(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);
          commit("REMOVE_BLOCKED_USER", payload);
          dispatch("FETCH_BLOCKED_USERS");
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    CLOSE_BLOCK_DIALOG({ commit }) {
      commit("SET_SHOW_DIALOG", false);
      commit("SET_USER_TO_BLOCK", null);
      commit("SET_REASON_TO_BLOCK", null);
    },
    CLOSE_UNBLOCK_DIALOG({ commit }) {
      commit("SET_SHOW_UNBLOCK_DIALOG", false);
      commit("SET_USER_TO_BLOCK", null);
    },
    SHOW_UNBLOCK_DIALOG({ commit }, payload = null) {
      if (payload) {
        commit("SET_USER_TO_BLOCK", payload);
      }
      commit("SET_SHOW_UNBLOCK_DIALOG", true);
    },
    TOGGLE_BLOCK_DIALOG({ commit }, payload = null) {
      if (payload) {
        commit("SET_USER_TO_BLOCK", payload);
      }
      commit("SET_SHOW_DIALOG", true);
    },
    CHANGE_REASON_FOR_BLOCK({ commit }, payload) {
      commit("SET_REASON_TO_BLOCK", payload);
    },
    ADD_REVEALED_CONTENT({ commit, state }, payload) {
      console.log("ADD_REVEALED_CONTENT", payload);
      if (
        !payload?.["entity"] ||
        !payload?.["slug"] ||
        typeof state.revealedContent?.[payload["entity"]] === "undefined"
      ) {
        console.log("Invalid payload");
        return false;
      }
      if (!state.revealedContent[payload["entity"]].includes(payload["slug"])) {
        commit("APPEND_REVEALED_CONTENT", payload);
      } else {
        console.log("Already revealed");
      }
    },
  },
};
