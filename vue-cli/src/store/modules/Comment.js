import Vue from "vue";

import axios from "axios";
export default {
  namespaced: true,
  state: {
    Fetched: [],
    NewComments: {},
    CommentsWithWYSIWYG: [],
    Comments: {},
    Debug: [],
    UIFeedbackMsg: "",
  },
  getters: {
    GET_COMMENT_DEBUG: (state) => {
      return state.Debug;
    },
    GET_COMMENTS_FETCHED: (state) => {
      return state.Fetched;
    },
    GET_COMMENTS_WITH_WYSIWYG: (state) => {
      return state.CommentsWithWYSIWYG;
    },
    GET_ALL_NEW_COMMENTS: (state) => {
      return state.NewComments;
    },
    GET_COMMENT_UI_FEEDBACK: (state) => {
      return state.UIFeedbackMsg;
    },
    GET_ALL_COMMENTS: (state) => {
      return state.Comments;
    },
    GET_COMMENTS: (state) => (identifier) => {
      return state.Comments?.[identifier];
    },
    GET_NEW_COMMENT_TEXT: (state) => (identifier) => {
      if (typeof state.NewComments[identifier] != "undefined") {
        return state.NewComments[identifier];
      } else {
        return "";
      }
    },
  },
  mutations: {
    ADD_DEBUG(state, payload) {
      state.Debug.push(payload);
    },
    RESET_COMMENTS_WITH_WYSIWYG(state) {
      state.CommentsWithWYSIWYG = [];
    },
    ADD_COMMENT_WITH_WYSIWYG(state, payload) {
      if (!state.CommentsWithWYSIWYG.includes(payload)) {
        state.CommentsWithWYSIWYG.push(payload);
      }
    },
    ADD_COMMENT_FETCHED(state, payload) {
      if (!state.Fetched.includes(payload)) {
        state.Fetched.push(payload);
      }
    },
    APPEND_COMMENT_LIST(state, payload) {
      let newList = [];
      let commentPK = payload.newComment["sk"];
      let commentsBySK = {};
      commentsBySK[commentPK] = payload.newComment;
      let sklist = [commentPK];
      let i;

      try {
        if (payload.identifier && state.Comments[payload.identifier]) {
          for (i in state.Comments[payload.identifier]) {
            let commentSK = state.Comments[payload.identifier][i]?.sk;
            commentsBySK[commentSK] = state.Comments[payload.identifier][i];
            sklist.push(commentSK);
          }
          sklist.sort();

          for (i in sklist) {
            let ikey = sklist[i];
            if (commentsBySK[ikey]) {
              newList.push(commentsBySK[ikey]);
            }
          }
          Vue.set(state.Comments, payload.identifier, newList);
        } else if (payload.identifier) {
          console.log("Cannot find comments for: " + payload.identifier);
        } else {
          console.log("No identifier");
        }
      } catch (err) {
        console.log(err);
      }
    },
    CYCLE_COMMENT_FETCHED(state, payload) {
      if (state.Fetched.includes(payload)) {
        delete state.Fetched[state.Fetched.indexOf(payload)];
        setTimeout(() => {
          state.Fetched.push(payload);
        }, 100);
      }
    },
    ADD_COMMENT_LIST(state, payload) {
      if (!state.Comments[payload.identifier]) {
        Vue.set(state.Comments, payload.identifier, []);
      }
      Vue.set(state.Comments, payload.identifier, payload.commentList);
      // this.$set(state.Comments, payload.identifier, payload.commentList);
    },
    UPDATE_COMMENT(state, payload) {
      Vue.set(
        state.Comments[payload.commentOn][payload.changeIndex],
        "commentText",
        payload.newText
      );
      // this.$set(state.Comments, payload.identifier, payload.commentList);
    },

    SET_NEW_COMMENT_TEXT(state, payload) {
      if (
        typeof payload.identifier != "undefined" &&
        typeof payload.commentText != "undefined"
      ) {
        state.NewComments[payload.identifier] = payload.commentText;
      }
    },
    SET_UI_FEEDBACK(state, payload) {
      state.UIFeedbackMsg = payload;
    },
  },
  actions: {
    ARCHIVE_COMMENT({ commit, rootGetters }, payload) {
      console.log(payload);
      var url = "";
      if (payload.userslug == rootGetters["UserStore/userSlug"]) {
        url = process.env.VUE_APP_APIBASEURL + "comment/archivecommentself";
      } else {
        url = process.env.VUE_APP_APIBASEURL + "comment/archive";
      }
      axios
        .post(url, payload, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);
          console.log("Deleted comment");
          // dispatch("DELETE_POST_ITEM", payload);
        })
        .catch(function (error) {
          commit("ADD_DEBUG", error);
        });
    },
    UPDATE_COMMENT_ITEM({ state, commit }, payload) {
      let i = 0;
      let changeIndex;
      for (i in state.Comments[payload.commentOn]) {
        if (state.Comments[payload.commentOn][i].slug == payload.slug) {
          changeIndex = i;
        }
      }
      if (changeIndex) {
        commit("UPDATE_COMMENT", {
          newText: payload.newText,
          commentOn: payload.commentOn,
          changeIndex: changeIndex,
        });
      }
    },
    RESET_COMMENT({ commit }, payload) {
      commit("SET_NEW_COMMENT_TEXT", {
        identifier: payload,
        commentText: "",
      });
    },
    COMMENT_WITH_WYSIYG_ON({ commit }, payload) {
      commit("ADD_COMMENT_WITH_WYSIWYG", payload);
    },
    UPDATE_NEW_COMMENT_TEXT({ commit }, payload) {
      commit("SET_NEW_COMMENT_TEXT", payload);
    },
    PUSH_COMMENT_CACHE({ commit }, payload) {
      if (!payload.commentOn.sk) {
        return false;
      }

      commit("ADD_COMMENT_FETCHED", window.makeIdentifier(payload.commentOn));
      commit("ADD_COMMENT_LIST", {
        identifier: window.makeIdentifier(payload.commentOn),
        commentList: payload.commentList,
      });
    },
    async FETCH_COMMENT_LIST({ commit, rootGetters }, payload) {
      var url;

      url =
        process.env.VUE_APP_APIBASEURL +
        "comment/ref-on/" +
        payload.sk.split("#").join("/");

      await axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          let commentList = {};
          if (typeof response.data.Items != "undefined") {
            commentList = response.data.Items;
          }
          let refon = { sk: payload.sk };
          commit("ADD_COMMENT_FETCHED", window.makeIdentifier(refon));
          commit("ADD_COMMENT_LIST", {
            identifier: window.makeIdentifier(refon),
            commentList: commentList,
          });
        })
        .catch(function (error) {
          commit("ADD_DEBUG", error);
        });
    },

    POST_COMMENT({ state, commit, dispatch, rootGetters }, payload) {
      let postdata = {
        parent: payload.commentOn,
        // topLevel: payload.topLevel,
        commentText:
          state.NewComments[window.makeIdentifier(payload.commentOn)],
      };
      if (
        state.NewComments[window.makeIdentifier(payload.commentOn)].length <= 1
      ) {
        return false;
      }
      
      var url = process.env.VUE_APP_APIBASEURL + "comment";
      axios
        .post(url, postdata, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          // dispatch("FETCH_COMMENT_LIST", payload);
          commit("SET_UI_FEEDBACK", "Posted!");
          setTimeout(() => {
            commit("SET_UI_FEEDBACK", "");
          }, 800);
          commit("APPEND_COMMENT_LIST", {
            identifier: window.makeIdentifier({
              sk: response.data["topLevel"]["sk"],
            }),
            newComment: response.data,
          });
          // commit("CYCLE_COMMENT_FETCHED", window.makeIdentifier(payload.commentOn));
          commit("ADD_DEBUG", response);
          commit("RESET_COMMENTS_WITH_WYSIWYG");

          // Now need to increase the stats for the post
          dispatch(
            "PostStore/UPDATE_POST_STAT",
            {
              action: "increment",
              statistic: "commentCount",
              slug: response.data["topLevel"].slug,
            },
            { root: true }
          );
          dispatch("RESET_COMMENT", window.makeIdentifier(payload.commentOn));

        })
        .catch(function (error) {
          commit("ADD_DEBUG", error);
        });
      // dispatch("FETCH_COMMENT_LIST", payload.commentOn);
    },
  },
};
