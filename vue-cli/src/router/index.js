import Vue from "vue";
import VueRouter from "vue-router";
import About from "../views/About.vue";
import RoadMap from "../views/RoadMap.vue";
import Contact from "../views/Contact.vue";
import store from "../store/index";
import EditCopy from "../views/CopyEditForm.vue";
import FAQS from "../views/FAQS.vue";
import BeginnersGuide from "../views/BeginnersGuide.vue";
import TrustLevels from "../views/TrustLevels.vue";

// import axios from "axios";
// import i18n from '../plugins/i18n';

Vue.use(VueRouter);
var routes;
window.paymentsOnlyMode = false;

window.setPageTitle = () => {
  console.log("setPageTitle", "This function is deprecated.")
  // document.title = "WTS2 - " + title;
};
routes = [
  {
    path: "/About",
    meta: {
      guestsidebar: true
    },
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG/About", params: { LANG: "en" } };
      } else {
        return { path: "/:LANG/About", params: { LANG: localStorage.locale } };
      }
    },
  },
  {
    path: "/:LANG/About",
    name: "About",
    component: About,
    meta: {
      guestsidebar: true
    },
  },
  {
    path: "/:LANG/RoadMap",
    name: "Road Map",
    component: RoadMap,
    meta: {
      guestsidebar: true
    },
  },

  {
    path: "/:LANG/editcopy",
    name: "EditCopy",
    component: EditCopy,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/:LANG/createbranch",
    name: "Create Branch",
    component: () =>
      import(/* webpackChunkName: "ReadPost" */ "../views/CreateSubwiki.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/Contact",
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG/Contact", params: { LANG: "en" } };
      } else {
        return {
          path: "/:LANG/Contact",
          params: { LANG: localStorage.locale },
        };
      }
    },
  },
  {
    path: "/:LANG/contact",
    name: "Contact",
    component: Contact,
    meta: {
      requiresAuth: false,
      allowUnconfirmed: true,
      guestsidebar: true
    },
  },
  {
    path: "/",
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG", params: { LANG: "en" } };
      } else {
        return { path: "/:LANG", params: { LANG: localStorage.locale } };
      }
    },
  },
  {
    path: "/auth",
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG/auth", params: { LANG: "en" } };
      } else {
        return { path: "/:LANG/auth", params: { LANG: localStorage.locale } };
      }
    },
  },
  {
    path: "/:LANG/auth",
    name: "Auth Page",
    meta: {
      requiresAuth: false,
      allowUnconfirmed: true,
      redirectAfterAuth: null
    },
    component: () =>
      import(/* webpackChunkName: "ReadPost" */ "../views/auth/AuthPage.vue"),
  },

  {
    path: "/:LANG/auth/forgotpassword",
    name: "Forgot Password",
    meta: {
      requiresAuth: false,
      allowUnconfirmed: true,
      guestsidebar: true
    },
    component: () =>
      import(
        /* webpackChunkName: "ReadPost" */ "../views/auth/ForgotPassword.vue"
      ),
  },
  {
    path: "/:LANG/auth/recoverpassword",
    name: "Password Recovery",
    meta: {
      requiresAuth: false,
      allowUnconfirmed: true,
    },
    component: () =>
      import(
        /* webpackChunkName: "EmailConfirmation" */ "../views/auth/PasswordRecovery.vue"
      ),
  },
  {
    path: "/:LANG/auth/emailconfirmation",
    name: "Email Confirmation",
    meta: {
      requiresAuth: true,
      allowUnconfirmed: true,
    },
    component: () =>
      import(
        /* webpackChunkName: "EmailConfirmation" */ "../views/auth/EmailConfirmation.vue"
      ),
  },
  {
    path: "/welcome",
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG/welcome", params: { LANG: "en" } };
      } else {
        return {
          path: "/:LANG/welcome",
          params: { LANG: localStorage.locale },
        };
      }
    },
  },
  {
    path: "/:LANG/welcome",
    name: "Welcome",
    meta: {
      requiresAuth: true,
      allowUnconfirmed: false,
    },
    component: () =>
      import(
        /* webpackChunkName: "EmailConfirmation" */ "../views/components/Welcome.vue"
      ),
  },
  {
    path: "/:LANG/auth/pleaseconfirm",
    name: "Confirm page",
    meta: {
      requiresAuth: true,
      allowUnconfirmed: true,
      guestsidebar: true
    },
    component: () =>
      import(
        /* webpackChunkName: "EmailConfirmation" */ "../views/auth/PleaseConfirm.vue"
      ),
  },

  {
    path: "/:LANG/auth/fromwts1/:token",
    name: "Auth From WTS1",
    meta: {
      requiresAuth: false,
    },
    component: () =>
      import(
        /* webpackChunkName: "AuthFromWTS1" */ "../views/auth/AuthFromWTS1.vue"
      ),
  },
  {
    path: "/postsbyid",
    name: "Posts By ID (Test)",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(
        /* webpackChunkName: "GetPostsByID" */ "../views/GetPostsByID.vue"
      ),
  },
  {
    path: "/:LANG/recentchanges",
    name: "Recent Changes",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(
        /* webpackChunkName: "GetPostsByID" */ "../views/RecentChanges.vue"
      ),
  },
  {
    path: "/:LANG/wt/:subwikiSlug",
    name: "Branch",
    meta: {
      // requiresAuth: true,
      title: ":subwikiSlug",
      guestsidebar: true

    },

    component: () =>
      import(/* webpackChunkName: "Branch" */ "../views/Subwiki.vue"),
  },
  {
    path: "/:LANG/post/:readSlug",
    name: "Read",
    meta: {
      // requiresAuth: true,
      sidebar: ["revisions"],   
      guestsidebar: true
   
    },
    component: () =>
      import(/* webpackChunkName: "ReadPost" */ "../views/ReadPost.vue"),
  },
  {
    path: "/:LANG/post/:readSlug/:revisionNo",
    name: "Read Revision",
    meta: {
      // requiresAuth: true,
      sidebar: ["revisions"],
    },
    component: () =>
      import(/* webpackChunkName: "ReadPost" */ "../views/ReadPost.vue"),
  },
  {
    path: "/:LANG/user/:userSlug",
    name: "User Profile",
    redirect: (to) => {
      // the function receives the target route as the argument
      // we return a redirect path/location here.
      return {
        name: "User Profile - Posts - Tab selected",
        params: {
          LANG: to.params.LANG,
          tabSelected: 'posts',
          userSlug: to.params.userSlug,
        },
      };
    },
  },

 
  {
    path: "/:LANG/user/:userSlug/posts/:tabSelected",
    name: "User Profile - Posts - Tab selected",
    meta: {   
      guestsidebar: true,
      viewMode: 'posts'
    },
    component: () =>
      import(/* webpackChunkName: "UserProfile" */ "../views/UserProfile.vue"),
  },
  {
    path: "/:LANG/user/:userSlug/trust",
    name: "User Profile - Trust",
    meta: {
      guestsidebar: true,
      viewMode: 'trust'
    },
    component: () =>
      import(/* webpackChunkName: "UserProfile" */ "../views/UserProfile.vue"),
  },
  {
    path: "/:LANG/user/:userSlug/trust/:radioSelected",
    name: "User Profile - Trust - Radio selected",
    meta: {
      guestsidebar: true,
      viewMode: 'trust'
    },
    component: () =>
      import(/* webpackChunkName: "UserProfile" */ "../views/UserProfile.vue"),
  },

  {
    path: "/:LANG/user/:userSlug/follow/:radioSelected",
    name: "User Profile - Follow - Radio selected",
    meta: {
      guestsidebar: true,
      viewMode: 'follow'
    },
    component: () =>
      import(/* webpackChunkName: "UserProfile" */ "../views/UserProfile.vue"),
  },

  {
    path: "/:LANG/editmyprofile",
    name: "Edit My Profile",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(/* webpackChunkName: "MyAccount" */ "../views/EditProfile.vue"),
  },
  {
    path: "/:LANG/myaccount",
    name: "My Account",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(/* webpackChunkName: "MyAccount" */ "../views/MyAccount.vue"),
  },
  {
    path: "/:LANG/myaccount/apiaccess",
    name: "API Access",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(/* webpackChunkName: "MyAccount" */ "../views/APIAccess.vue"),
  },
  {
    path: "/:LANG/myaccount/userblocks",
    name: "User Blocks",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(/* webpackChunkName: "MyAccount" */ "../views/UserBlocks.vue"),
  },
  {
    path: "*",
    name: "404 - Page not 'here'",
    component: () =>
      import(/* webpackChunkName: "MyPayments" */ "../views/404.vue"),
  },
  {
    path: "/faqs",
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG/faqs", params: { LANG: "en" } };
      } else {
        return { path: "/:LANG/faqs", params: { LANG: localStorage.locale } };
      }
    },
  },
  {
    path: "/:LANG/faqs",
    name: "FAQS",
    component: FAQS,
    meta: {
      requiresAuth: false,
      guestsidebar: true
    },
  },
  {
    path: "/post-restrictions",
    redirect() {
      if (typeof localStorage.locale != "string") {
        return { path: "/:LANG/post-restrictions", params: { LANG: "en" } };
      } else {
        return { path: "/:LANG/post-restrictions", params: { LANG: localStorage.locale } };
      }
    },
  },
  {
    path: "/:LANG/post-restrictions",
    name: "Post Restrictions",
    component: () =>
      import(/* webpackChunkName: "MyAccount" */ "../views/PostRestrictions.vue"),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "/:LANG/guide",
    name: "BeginnersGuide",
    component: BeginnersGuide,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/:LANG/trust",
    name: "TrustLevels",
    component: TrustLevels,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "/:LANG/branches",
    redirect: (to) => {
      // the function receives the target route as the argument
      // we return a redirect path/location here.
      return {
        name: "Branches - Radio Selected",
        params: {
          LANG: to.params.LANG,
          userSlug: to.params.userSlug,
          radioSelected: "alphabetical"
        },
      };
    },
  },
  {
    path: "/:LANG/branches/:radioSelected",
    name: "Branches - Radio Selected",
    meta: {
      requiresAuth: true,
      allowUnconfirmed: false,
    },
    component: () =>
      import(
        /* webpackChunkName: "EmailConfirmation" */ "../views/BrowseSubwikis.vue"
      ),
  },
  // {
  //   path: "/:LANG/mybranches",
  //   name: "My Branches",
  //   meta: {
  //     requiresAuth: true,
  //     allowUnconfirmed: false,
  //   },
  //   component: () =>
  //     import(
  //       /* webpackChunkName: "EmailConfirmation" */ "../views/components/MySubwikis.vue"
  //     ),
  // },
  {
    path: "/:LANG",
    name: "Home",
    meta: {
      requiresAuth: true,
    },
    component: () =>
      import(/* webpackChunkName: "ReadPost" */ "../views/Home.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

router.beforeEach(async (to, from, next) => {

  let authenticated = await store.dispatch("UserStore/fetchAuthenticatedUser");
  if (authenticated) {

    let supportedlanguages = ['en', 'es', 'de', 'pt', 'ru']
    if (supportedlanguages.includes(to.params.LANG)){
      store.dispatch("Languages/UPDATE_CURRENT_LOCALE", to.params.LANG, { root: true });
    }
  }
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // console.log(
    //   "# PRE FLIGHT CHECK ROUTER BEFORE-EACH - IS THERE A RECORD THAT MATCHES THE INPUT GIVEN?"
    // );
    try {
      // let authenticated = await store.dispatch(
      //   "UserStore/fetchAuthenticatedUser"
      // );
      if (authenticated) {
        // console.log("#ROUTER BEFORE-EACH - THE PERSON IS AUTHENTICATED");
        if (!to.matched.some((record) => record.meta.allowUnconfirmed)) {
          // console.log(
          //   "#ROUTER BEFORE-EACH - THIS PAGE DOES NOT ALLOW UNCONFIRMED"
          // );
          let confirmed = await store.getters["UserStore/isEmailVerified"];
          if (confirmed) {
            // console.log(
            //   "#ROUTER BEFORE-EACH - THE PERSON IS APPARENTLY CONFIRMED"
            // );
            store.dispatch(
              "SocketStore/SOCKET_SEND_MESSAGE",
              {
                action: "CLIENT_CHANGE_PAGE",
                payload: {
                  path: to.fullPath,
                },
              },
              { root: true }
            );
            next();
          } else if (this.authenticated.userID != 'guest'){
            // console.log(
            //   "#ROUTER BEFORE-EACH - THE PERSON IS NOT CONFIRMED WHERE ARE WE GOING?"
            // );
            next({
              name: "Confirm page",
              params: { LANG: store.getters["UserStore/userLanguage"] },
            });
          }
        } else {
          // console.log(
          //   "#ROUTER BEFORE-EACH - UNCONFIRMED PEOPLE CAN VIEW THIS PAGE"
          // );
          next();
        }
      } else {
        // console.log("#ROUTER BEFORE-EACH - THIS PERSON IS NOT AUTHENTICATED");


        if (typeof localStorage.locale != "string") {
          next({
            name: "Auth Page",
            params: { LANG: "en" , redirectAfterAuth: to.fullPath},
          });
        } else {
          next({
            name: "Auth Page",
            params: { LANG: localStorage.locale, redirectAfterAuth: to.fullPath },
          });
        }
      }
    } catch (e) {
      console.log("Error fetching user", e);
    }
  } else {
    next();
  }
});
export default router;
