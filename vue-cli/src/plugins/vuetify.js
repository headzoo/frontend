import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import "font-awesome/css/font-awesome.min.css"; // Ensure you are using css-loader

Vue.use(Vuetify);

const wts2colors = {
  blue: "#5f91fc",
  navblue: "#4bb1ff",
  green1: "#35c71f",
  green2: "#9acf13",
  pink: "#f567c5",
  red: "#ff4e4e",
  black: "#191a1a",
  grey: "#f3f3f3",
  lightgrey: "#bfbbbb",
  darkgrey: "#4a4f52",
  darkbtn: "#88b811",
  darkgreybutton: "#363636",
  navbackgrounddark: "#363636",
  navbackgroundlight: "#f5f5f5"
};

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: wts2colors.blue,
        secondary: wts2colors.green2,
        // accent: "#8c9eff",
        // error: "#b71c1c",
        wtsblue: wts2colors.blue,
        wtsnavblue: wts2colors.navblue,
        wtsgreen1: wts2colors.green1,
        wtsgreen2: wts2colors.green2,
        wtspink: wts2colors.pink,
        wtsred: wts2colors.red,
        wtsblack: wts2colors.black,
        wtslightgrey: wts2colors.lightgrey,
        wtsgrey: wts2colors.grey,
        wtsdarkgrey: wts2colors.darkgrey,
        wtsnavbackground: wts2colors.navbackgroundlight,
        wts2panelcolor: '#f3f3f3'
      },
      dark: {
        primary: wts2colors.blue,
        secondary: wts2colors.green2,
        // anchor: 'white',
        wtsblue: wts2colors.blue,
        wtsnavblue: wts2colors.navblue,
        wtsgreen1: wts2colors.green1,
        wtsgreen2: wts2colors.green2,
        wtspink: wts2colors.pink,
        wtsred: wts2colors.red,
        wtslightgrey: wts2colors.lightgrey,
        wtsdarkgrey: wts2colors.darkgrey,
        wtsblack: wts2colors.black,
        wtsdarkbtn: wts2colors.darkbtn,
        wtsdarkgreybutton: wts2colors.darkgreybutton,
        wtsnavbackground: wts2colors.navbackgrounddark,
        wts2panelcolor: '#1E1E1E'
      },
    },
  },
});
