window.dateOut = function (dateIn) {
  // console.log(dateIn);
  let d = new Date(dateIn);
  return (
    d.getDate() +
    "/" +
    (d.getMonth() + 1) +
    "/" +
    d.getFullYear() +
    "  @ " +
    d.getHours() +
    ":" +
    window.numpad(d.getMinutes(), 2)
  );
};
window.numpad = function (n, width, z = 0) {
  if (typeof n == "number") {
    n = n.toString();
  }
  z = z || "0";
  n = n + "";
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

window.getCookie = function (name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};
window.setCookie = function (cname, cvalue, exdays, intradomain = false) {
  // console.log("Setting cookie....", { cname, cvalue, exdays, intradomain });
  try {

    const d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    let expires = "expires=" + d.toUTCString();
    let newcookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    if (intradomain) {
      newcookie += ";domain=" + process.env.VUE_APP_INTRA_DOMAIN_COOKIE;
    }
    console.log("newcookie", newcookie);
    document.cookie = newcookie;
  } 
  catch(e) {
    console.log("An error occured while setting cookie");
    console.log(e);
  }
};
window.eraseCookie = function (cname, intradomain = false) {
  let expires = "expires=Thu, 01 Jan 1970 00:00:01 GMT";
  let newcookie = cname + "= ;" + expires + ";path=/";
  if (intradomain) {
    newcookie += ";domain=" + process.env.VUE_APP_INTRA_DOMAIN_COOKIE;
  }
  document.cookie = newcookie;
};
// window.eraseCookie = function (name, intradomain = false) {

//   window.setCookie(name, "", -99999999, intradomain);
// };

window.makeIdentifier = (payload) => {
  return "commentOn-" + payload?.sk;
};
window.isElementVisible = (elementId) => {
  var element = document.getElementById(elementId);
  if (element) {
    var rect = element.getBoundingClientRect();
    var html = document.documentElement;
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || html.clientHeight) &&
      rect.right <= (window.innerWidth || html.clientWidth)
    );
  } else {
    console.log("No element");
  }
  return false;
};

window.timeSince = (date) => {
  var seconds = Math.floor((new Date() - date) / 1000);

  var interval;
  var intervals = {
    "year": 31536000,
    "month": 2592000,
    "day": 86400,
    "hour": 3600,
    "minute": 60,
    "second": 1
  }
  let i;
  let keys = Object.keys(intervals);

  for (i in keys) {
    let label = keys[i];
    interval = seconds / intervals[label];
    if (interval > 1) {
      if (interval >= 2) {
        label += 's';
      }
      return Math.floor(interval) + " " + label;
    }
  }
  // var interval = seconds / 31536000;


  // if (interval > 1) {
  //   return Math.floor(interval) + " years";
  // }
  // interval = seconds / 2592000;
  // if (interval > 1) {
  //   return Math.floor(interval) + " months";
  // }
  // interval = seconds / 86400;
  // if (interval > 1) {
  //   return Math.floor(interval) + " days";
  // }
  // interval = seconds / 3600;
  // if (interval > 1) {
  //   return Math.floor(interval) + " hours";
  // }
  // interval = seconds / 60;
  // if (interval > 1) {
  //   return Math.floor(interval) + " minutes";
  // }
  // return Math.floor(seconds) + " seconds";
};

window.dynamicSort = ( property) => {
  var sortOrder = 1;
  if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
  }
  return function (a,b) {
      /* next line works with strings and numbers, 
       * and you may want to customize it to your needs
       */
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
  }
}